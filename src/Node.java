
import java.util.*;

import static java.lang.String.format;
// viide: https://pastebin.com/ZsH0G5eh
// viide: https://git.wut.ee/i231/home5/commit/e98832db0e1292bb6bb952aab1905e9db1aa5fd2
// viide: https://bitbucket.org/gagarinerik/kt5/src/master/

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    private String getName() {
        return name;
    }

    private void setFirstChild(Node firstChild) {
        this.firstChild = firstChild;
    }

    private Node getFirstChild() {
        return firstChild;
    }

    private void setNextSibling(Node nextSibling) {
        this.nextSibling = nextSibling;
    }

    private Node getNextSibling() {
        return nextSibling;
    }

    private static void checkString(String s) {
        if (s.isEmpty()) {
            throw new RuntimeException(format("Empty expression: '%s'", s));
        } else if (s.contains(",,")) {
            throw new RuntimeException(format("Invalid input, double commas in expression %s", s));
        } else if (s.contains(",)") || s.contains("(,")) {
            throw new RuntimeException(format("Invalid input, no siblings in expression %s", s));
        } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
            throw new RuntimeException(format("Invalid input, no brackets in expression %s", s));
        } else if (s.contains("()")) {
            throw new RuntimeException(format("Invalid input, empty subtree in expression %s", s));
        } else if (s.contains("))")) {
            throw new RuntimeException(format("Invalid input, double bracket in expression %s", s));
        }
    }

    public static Node parsePostfix(String s) {

      checkString(s);

//        try {
//            checkString(s);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        Stack<Node> stk = new Stack<>();
        Node last = new Node(null, null, null);
        boolean rootClosed = false;

        StringTokenizer tok = new StringTokenizer(s, "(),", true);

        while (tok.hasMoreTokens()) {
            String w = tok.nextToken().trim();

            if (w.isEmpty()) {
                throw new RuntimeException(format("Empty expression: '%s'", s));
            }
            if (w.contains("\t")) {
                throw new RuntimeException(format("Invalid input, tab character in expression %s", s));
            }
            if (w.contains(" ")) {
                throw new RuntimeException(format("Invalid input, space character in expression '%s'", s));
            }

            if (w.equals("(")) {
                if (rootClosed) {
                    throw new RuntimeException(format("Root element was already closed '%s'", s));
                }
                stk.push(last);
                last.setFirstChild(new Node(null, null, null));
                last = last.getFirstChild();

            } else if (w.equals(",")) {
                if (rootClosed) {
                    throw new RuntimeException(format("Root element was already closed '%s'", s));
                }
                last.setNextSibling(new Node(null, null, null));
                last = last.getNextSibling();

            } else if (w.equals(")")) {
                last = stk.pop();
                if (stk.size() == 0) {
                    rootClosed = true;
                }
            } else {
                last.name = last.name == null ? "" + w : last.name + w;
            }
        }
        //https://stackoverflow.com/questions/17302256/best-way-to-check-for-null-values-in-java
        if (last.getName() == null) {
            throw new RuntimeException(format("Invalid Node name: '%s'", s));
        }
        if (last.getNextSibling() != null) {
            throw new RuntimeException(format("Two root elements from string: '%s'", s));
        }
        if (last.getFirstChild() != null && last.getFirstChild().getName() == null) {
            throw new RuntimeException(format("Child Node name missing. String: '%s'", s));
        }
        return last;
    }

    public String leftParentheticRepresentation() {
        StringBuffer b = new StringBuffer();
        b.append(getName());
        if (getFirstChild() != null) {
            b.append("(");
            b.append(getFirstChild().leftParentheticRepresentation());
            b.append(")");
        }
        if (getNextSibling() != null) {
            b.append(",");
            b.append(getNextSibling().leftParentheticRepresentation());
        }
        return b.toString();
    }

    public static void main(String[] param) {
        String s = "%%";            // ok
        // String s = "((@,#)+)-34";   // ok
        // String s = "((@, #)+)-34";  // ok
        // String s = "     \t ";      // not ok
        // String s = "((1),(2)3)4";   // not ok
        // String s = "A B";           // not ok
        // String s = "((3 4, 5)6)7";  // not ok
        System.out.println("Input: " + s);
        Node tree = Node.parsePostfix(s);
        System.out.println(" Left: "
                + tree.leftParentheticRepresentation());
    }
}


